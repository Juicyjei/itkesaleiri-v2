/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkesa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author n1682
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField Textfield;
    @FXML
    private Button saveButton;
    @FXML
    private Button openButton;
    @FXML
    private TextArea textarea;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void openButtonAction(ActionEvent event) {
        String text = Textfield.getText();
        String s; 
        try {
            BufferedReader br = new BufferedReader(new FileReader(text));
            while ((s = br.readLine()) != null){
                textarea.appendText(s + "\n");
                
            }
            br.close();
        } catch (FileNotFoundException ex){
                System.out.println("Tiedostoa ei löytynyt.");
        } catch (IOException ex){
            System.err.println("Tiedoston luku ei onnistunut."); 
        }
    }
    

    @FXML
    private void saveButtonAction(ActionEvent event) {
        String text = Textfield.getText();
        String teksti = textarea.getText();
        try {
            BufferedWriter w = new BufferedWriter(new FileWriter(Textfield.getText()));
                w.write(teksti);
                Textfield.clear();
                textarea.clear();
   
            w.close();
        } catch (FileNotFoundException ex){
                System.out.println("Tiedostoa ei löytynyt.");
        } catch (IOException ex){
            System.err.println("Tiedoston luku ei onnistunut."); 
        }
    }
}
