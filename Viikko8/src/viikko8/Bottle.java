/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8;


public class Bottle {
    
    private String name;
    private String valmistaja;
    private String tila;
    private double hinta;
    
    public Bottle(){
        
        name = "Pepsi Max";
        valmistaja = "Pepsi";
        tila = "0.5";
        hinta = 1.80;
        
    }
    public Bottle(String n, String t, double h){
        name = n;
        tila = t;
        hinta = h;
        
    }
    public String getName(){
        return name;
    }
    public double getMoney(){
        return hinta;
    }
    public String getSize(){
        return tila;
    }
    
}
