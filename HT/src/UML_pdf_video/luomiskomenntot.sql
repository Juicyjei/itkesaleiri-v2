Jussi Moilanen
Listatut tietokannan luomiskomennot, insert lauseet ja sql kyselyt:


CREATE TABLE IF NOT EXISTS "sijainti" (
    "sijaintiID" INTEGER    PRIMARY KEY,
    "postinumero" INTEGER   NOT NULL,
    "paikkakunta" VARCHAR(32)   NOT NULL,
    "osoite"    VARCHAR(32)     NOT NULL,
    "latitude"      CHAR (10)   NOT NULL,
    "longitude"     CHAR (10)   NOT NULL,
    
    CHECK ("sijaintiID" > 0)

);

CREATE TABLE IF NOT EXISTS"postiautomaatti" (
    "automaattiID"  INTEGER     PRIMARY KEY,
    "sijaintiID"    INTEGER     NOT NULL,
    "aukioloaika"   VARCHAR(64) NOT NULL,
    "postoffice"    VARCHAR(64) NOT NULL,

    FOREIGN KEY ("sijaintiID") REFERENCES "sijainti" ("sijaintiID") ON DELETE CASCADE,

    CHECK ("automaattiID" > 0)
);

CREATE TABLE IF NOT EXISTS"luokka" (
    "luokkaID"  INTEGER     PRIMARY KEY,
    "matkaraja" INTEGER,
    "painoraja" INTEGER,
    "kokorajaX" INTEGER,
    "kokorajaY" INTEGER,
    "kokorajaZ" INTEGER,

    CHECK ("painoraja" > 0),
    CHECK ("matkaraja" > 0)

);

CREATE TABLE IF NOT EXISTS"esine" (
    "esineID"   INTEGER     PRIMARY KEY,
    "nimi"      VARCHAR(32) NOT NULL,
    "korkeus"   INTEGER,
    "leveys"    INTEGER,
    "syvyys"    INTEGER,
    "paino"     INTEGER,
    "getbroken" BOOLEAN,

    CHECK ("esineID" > 0),
    CHECK ("leveys" > 0),
    CHECK ("korkeus" > 0),
    CHECK ("syvyys" > 0),
    CHECK ("paino" > 0)
);

CREATE TABLE IF NOT EXISTS"paketti" (
    "pakettiID" INTEGER     PRIMARY KEY,
    "esineID"   INTEGER,
    "luokkaID"  INTEGER,
    "startpost" VARCHAR(32),
    "endpost"   VARCHAR(32), 
        "matkanpituus" REAL,
    
    FOREIGN KEY ("startpost") REFERENCES "postiautomaatti" ("automaattiID") ON DELETE CASCADE,
    FOREIGN KEY ("endpost") REFERENCES "postiautomaatti" ("automaattiID") ON DELETE CASCADE,
    FOREIGN KEY ("esineID") REFERENCES "esine" ("esineID" ) ON DELETE CASCADE,
    FOREIGN KEY ("luokkaID") REFERENCES "luokka" ("luokkaID") ON DELETE CASCADE,

    CHECK ("pakettiID" > 0),
    CHECK ("esineID" > 0),
    CHECK ("luokkaID" > 0)
    
);

CREATE TABLE IF NOT EXISTS"paketitautomaatissa" (
    "pakettiID" INTEGER,
    "automaattiID"  INTEGER,

    PRIMARY KEY ("pakettiID", "automaattiID")
);

CREATE TABLE IF NOT EXISTS"varasto" (
    "pakettiID" INTEGER PRIMARY KEY,
    "varastoID" VARCHAR,

    FOREIGN KEY ("pakettiID") REFERENCES "paketti" ("pakettiID") ON DELETE CASCADE
);

Insert lauseet:

INSERT INTO sijainti" + " VALUES (" + (i + 1) + ","
                        + parsecontent.getSmartPostList().get(i).getPostalcode() + ",'"
                        + parsecontent.getSmartPostList().get(i).getCity() + "','"
                        + parsecontent.getSmartPostList().get(i).getAddress() + "','"
                        + parsecontent.getSmartPostList().get(i).getGeoPoint().getLat() + "','"
                        + parsecontent.getSmartPostList().get(i).getGeoPoint().getLng() + "')");
                st.executeUpdate(String.format("INSERT INTO postiautomaatti VALUES (%s,%s,'%s','%s')", (i + 1), (i + 1),
                        parsecontent.getSmartPostList().get(i).getAvailability(), parsecontent.getSmartPostList().get(i).getPostOffice()));


"INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 1, "ONE PLUS 6 128GB", 8, 8, 15, 1, "TRUE"));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 2, "Perry Miesten Canvas kengät", 15, 15, 30, 1, "FALSE"));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 3, "Macbook Pro 13 Tähtiharmaa", 8, 30, 30, 2, "TRUE"));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 4, "Pioneer SX-S30DAB Pre-Out + Genelec G Three", 15, 15, 20, 3, "TRUE"));
"INSERT OR IGNORE INTO esine VALUES "
                    + "(%s,'%s',%s,%s,%s,%s,%b)", null, name, lenght, width, height, weight, broken));
"INSERT OR IGNORE INTO luokka VALUES "
                    + "(%s,%s,%s,%s,%s,%s)", null, 150, 250, 125, 125, 125));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO luokka VALUES "
                    + "(%s,%s,%s,%s,%s,%s)", null, 1500, 25, 50, 50, 50));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO luokka VALUES "
                    + "(%s,%s,%s,%s,%s,%s)", null, 1500, 250, 125, 125, 125));
"INSERT INTO paketti VALUES "
                        + "(%s,%s,%s,'%s','%s',%s)", null, getEsineID(esinenimi), classobject, startpost, endpost, matkanpituus));
                String lastrowid = "last_insert_rowid()";
                st.executeUpdate(String.format("INSERT OR IGNORE INTO varasto VALUES "
                        + "(%s,%s)", lastrowid, 1));
                System.out.println("Paketti lisätty varastoon");
SQL kyselyt:

"SELECT nimi from esine;"
"delete from esine where nimi = '" + esinenimi + "';"
"delete from paketti where pakettiID = '" + pakettiID + "';";
 "delete from varasto where pakettiID = '" + pakettiID + "';";
"SELECT esineID FROM esine WHERE nimi = '" + esinenimi + "';";
"select COUNT(*) from paketti;"
"SELECT latitude, longitude  from sijainti INNER JOIN"
                    + " postiautomaatti ON sijainti.sijaintiID = postiautomaatti."
                    + "sijaintiID WHERE postoffice = '" + startpost1 + "';";
"SELECT latitude, longitude  from sijainti INNER JOIN"
                    + " postiautomaatti ON sijainti.sijaintiID = postiautomaatti."
                    + "sijaintiID WHERE postoffice = '" + endpost1 + "';";
"SELECT osoite, postinumero FROM sijainti WHERE paikkakunta = '" + paikkakunta + "';";

"SELECT getbroken FROM esine"
                    + " WHERE nimi = '" + esinenimi + "';";
"SELECT luokkaID FROM paketti WHERE pakettiID = '" + pakettiID + "';"

"SELECT paino FROM esine WHERE nimi = '" + esinenimi + "';";

"SELECT aukioloaika, postoffice FROM postiautomaatti "
                    + "INNER JOIN sijainti ON sijainti.sijaintiID = "
                    + "postiautomaatti.sijaintiID WHERE paikkakunta = '" + paikkakunta + "';";

"SELECT nimi, pakettiID, startpost, endpost, matkanpituus from esine INNER JOIN paketti ON "
                    + "paketti.esineID = esine.esineID WHERE paketti.esineID = esine.esineID;";

"SELECT postoffice from postiautomaatti "
                    + "INNER JOIN sijainti On sijainti.sijaintiID = postiautomaatti.sijaintiID"
                    + " WHERE paikkakunta = '%s'", paikkakunta);
"SELECT postoffice from postiautomaatti "
                    + "INNER JOIN sijainti On sijainti.sijaintiID = postiautomaatti.sijaintiID"
                    + " WHERE paikkakunta = '%s'", paikkakunta);
