/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import javafx.scene.transform.Rotate;

/**
 *
 * @author Jussi
 */
public class ShowCube {
    
    Box box = new Box();
    
    final PhongMaterial Material = new PhongMaterial();   
    
    public ShowCube() {        
        
        box.setTranslateX(120);
        box.setTranslateY(100);
        Point3D point = new Point3D(0, 0, 0);
    }
    
    public Group makeCube(double valueX, double valueY, double valueZ, int maxsize) {
        box.setWidth(valueX);
        box.setHeight(valueY);        
        box.setDepth(valueZ);
        
        // Sets material color to red if box is too large
        if ((valueX > maxsize) || (valueY > maxsize) || (valueZ > maxsize)) {
            Material.setSpecularColor(Color.RED);
            Material.setDiffuseColor(Color.FIREBRICK);
            box.setMaterial(Material);
        } else {
            Material.setDiffuseColor(Color.BISQUE);
            Material.setSpecularColor(Color.BEIGE);
            box.setMaterial(Material);    
        }
        // Set rotations to cube
        Rotate rxBox = new Rotate(0, 0, 0, 0, Rotate.X_AXIS);
        Rotate ryBox = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
        Rotate rzBox = new Rotate(0, 0, 0, 0, Rotate.Z_AXIS);
        rxBox.setAngle(20);
        ryBox.setAngle(20);
        rzBox.setAngle(0);
        box.getTransforms().addAll(rxBox, ryBox, rzBox);
        Group cube = new Group(box);
        return cube;
    }
    
    public Group makeClassCube(double leveys,double korkeus,double syvyys) {
        Box linebox = new Box(leveys, korkeus, syvyys);
        // Surrouding cube with only lines
        linebox.setTranslateX(120);
        linebox.setTranslateY(100);
        Rotate rxBox = new Rotate(0, 0, 0, 0, Rotate.X_AXIS);
        Rotate ryBox = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
        rxBox.setAngle(20);
        ryBox.setAngle(20);
        linebox.getTransforms().addAll(rxBox, ryBox);
        linebox.setDrawMode(DrawMode.LINE);
        Group linecube = new Group(linebox);
        return linecube;
    }
}
