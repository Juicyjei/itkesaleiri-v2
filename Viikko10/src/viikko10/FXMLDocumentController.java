/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;


import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;


/**
 * 11.6.2018
 * Jussi Moilanen
 */
public class FXMLDocumentController implements Initializable {

    ArrayList<String> selain;
    String http = "https://";
    int x = 0;
    
    @FXML
    private WebView web;
    @FXML
    private GridPane uppanel;
    @FXML
    private TextField inputtext;

    public FXMLDocumentController() {
        this.selain = new ArrayList<>();
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        web.getEngine().load("http://www.lut.fi");
        selain.add("lut.fi");
        System.out.println(selain);
        
    }    

    @FXML
    private void BackAction(ActionEvent event) {
        if (selain.isEmpty()){
            web.getEngine().reload();
        }else{
            web.getEngine().load(http + selain.get(x-1));
            System.out.println(selain.get(x-1));
            x--;
        }
        
    }

    @FXML
    private void ForwardAction(ActionEvent event) {
        if (selain.isEmpty()){
            web.getEngine().reload();
            
        }else{
            web.getEngine().load(http + selain.get(x+1));
            System.out.println(selain.get(x+1));
            x++;
        }
    }

    @FXML
    private void InputTextAction(ActionEvent event) {
        
        String input = inputtext.getText();
        
        
        if (input.startsWith("http://") || input.startsWith("https://")){
            web.getEngine().load(input);
        }else{
            web.getEngine().load(http + input);
        }
        
        
        
        if (input.equals("index.html")){
            web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        }

        selain.add(input);
        x++;
        selain.subList(x, selain.size()-1).clear();


    }

    @FXML
    private void RefreshAction(ActionEvent event) {   
        web.getEngine().reload();
    }

    @FXML
    private void RunScriptAction(ActionEvent event) {
        
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void InitializeScriptAction(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }
    
}
