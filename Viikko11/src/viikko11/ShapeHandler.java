/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;


import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;


public class ShapeHandler {
    
    
    static private ShapeHandler sh;
    private List<Circle> dotlist;
    private List<Line> linelist;
    
    public ShapeHandler(){
        
        dotlist = new ArrayList<Circle>();
        linelist = new ArrayList<Line>();
    }
    
    public Line drawline(double x1, double y1, double x2, double y2){
        
        Line line = new Line();
        
        line.setStartX(x1);
        line.setStartY(y1);
        line.setEndX(x2);
        line.setEndY(y2);
        line.setStroke(Color.RED);
        line.setStrokeWidth(2);
        linelist.add(line);
        System.out.println(line);
        return line;
    }
    public static ShapeHandler getInstance(){
        
        if (sh == null){
            sh = new ShapeHandler();
        }
        return sh;
    }
    
    
    public List<Circle> getList(){
        return dotlist;
    }
    public List<Line> getLine(){
        return linelist;
    }
}
