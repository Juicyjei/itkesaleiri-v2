/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

/**
 *
 * @author n1682
 */
public class Paketti {
    private final String nimi;
    private final int pakettiID;
    private final String startpost;
    private final String endpost;
    private final double matkanpituus;
    
        public Paketti(String nimi, int pakettiID,String start,String end,double matkanpituus){
            this.nimi = nimi;
            this.pakettiID = pakettiID;
            startpost = start;
            endpost = end;
            this.matkanpituus = matkanpituus;
        }
        
        public String getNimi(){
            return nimi;
        }      
        public String getStartpost(){
            return startpost;
        }
        public String getEndpost(){
            return endpost;
        }
        public int getPakettiID(){
            return pakettiID;
        }
        public double getMatkanpituus(){
            return matkanpituus;
        }
        
    @Override
        public String toString(){
            return nimi;
        }
}
