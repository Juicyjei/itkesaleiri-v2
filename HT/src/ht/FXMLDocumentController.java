/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author n1682
 */
public class FXMLDocumentController implements Initializable {

    MainClass main = MainClass.getInstance();
    TableView<Paketti> tablev;

    private Label label;
    @FXML
    private WebView map;
    @FXML
    private ChoiceBox<String> pickcity;
    @FXML
    private Button openWindowButtom;
    @FXML
    private ComboBox<Paketti> readyPackets;
    @FXML
    private Label PacketInfoText;
    @FXML
    private AnchorPane MapTab;
    @FXML
    private Label packetAmount;
    @FXML
    private VBox vbox;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        map.getEngine().load(getClass().getResource("Kartta.html").toExternalForm());
        pickcity.setItems(main.getCitys());
        pickcity.getSelectionModel().selectFirst();
        readyPackets.setItems(main.getPacketsList());
        readyPackets.setStyle("-fx-font-size: 18px;");
        main.addItemData();
       // main.addClassDatabase();

    }

    @Override
    public String toString() {
        return "FXMLDocumentController{" + "label=" + label + ", kartta=" + map + '}';
    }

    @FXML
    private void openPacketWindow(ActionEvent event) {

        try {
            Stage window = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("FXMLPacketwindow.fxml"));

            Scene scene = new Scene(root);
            String css = this.getClass().getResource("Style.css").toExternalForm();
            scene.getStylesheets().add(css);
            window.setScene(scene);
            window.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void AddSmartPostAction(ActionEvent event) {

        String paikkakunta = pickcity.getSelectionModel().getSelectedItem();
        ArrayList<String> searchList = main.getSearchList(paikkakunta);
        ArrayList<String> smartPostInfo = main.getSmartPostInfo(paikkakunta);
        for (int i = 0; i < searchList.size(); i++) {
            map.getEngine().executeScript("document.goToLocation('" + searchList.get(i) + "," + paikkakunta + "','"
                    + smartPostInfo.get(i) + "','red')");

        }
        searchList.clear();
        smartPostInfo.clear();

    }

    @FXML
    private void createPathOnMap(ActionEvent event) {
        //ArrayList pathArray = main.getPathArray(startpost, endpost);
        if (readyPackets.getSelectionModel().isEmpty()) {
            PacketInfoText.setText("Valitse paketti ensin.");
            PacketInfoText.setStyle("-fx-font-size: 16px;");
        } else {
            String startpost1 = readyPackets.getSelectionModel().getSelectedItem().getStartpost();
            String endpost1 = readyPackets.getSelectionModel().getSelectedItem().getEndpost();
            String esinenimi = readyPackets.getSelectionModel().getSelectedItem().getNimi();
            System.out.println(main.getPathArray(startpost1, endpost1));
            String s = "document.createPath(" + main.getPathArray(startpost1, endpost1).toString()
                    + ",'red',1)";
            System.out.println(s);
            map.getEngine().executeScript(s);

            int pakettiID = readyPackets.getSelectionModel().getSelectedItem().getPakettiID();
            if (main.getBrokenPacket(esinenimi, pakettiID)) {
                PacketInfoText.setText("Paketti meni rikki matkan aikana.");
                PacketInfoText.setStyle("-fx-text-fill: red; -fx-font-size: 16px;");
            } else {
                PacketInfoText.setText("Lähetys meni perille.");
                PacketInfoText.setStyle("-fx-text-fill: green; -fx-font-size: 16px;");
            }
            main.deleteSelectedPacket(pakettiID);
            readyPackets.setItems(main.getPacketsList());
        }
    }

    @FXML
    private void deletePacketAction(ActionEvent event) {
        if (readyPackets.getSelectionModel().isEmpty()) {
            PacketInfoText.setText("Valitse paketti ensin.");
            PacketInfoText.setStyle("-fx-font-size: 16px;");
        } else {
            int pakettiID = readyPackets.getSelectionModel().getSelectedItem().getPakettiID();
            main.deleteSelectedPacket(pakettiID);
            readyPackets.setItems(main.getPacketsList());
            PacketInfoText.setText("Paketti poistettu.");
            PacketInfoText.setStyle("-fx-text-fill: green; -fx-font-size: 16px;");
        }
    }

    @FXML
    private void DeletePaths(ActionEvent event) {

        String script = "document.deletePaths()";
        map.getEngine().executeScript(script);
        System.out.println("Reitit tyhjennetty.");
    }

    @FXML
    private void tabChangedAction(Event event) {

        vbox.getChildren().clear();
        //esine column
        TableColumn<Paketti, String> esinecolumn = new TableColumn<>("Esine");
        esinecolumn.setMinWidth(300);
        esinecolumn.setCellValueFactory(new PropertyValueFactory<Paketti, String>("nimi"));
        // Starpost column
        TableColumn<Paketti, String> startpostcolumn = new TableColumn<>("Lähtöposti");
        startpostcolumn.setMinWidth(300);
        startpostcolumn.setCellValueFactory(new PropertyValueFactory<Paketti, String>("startpost"));
        // Endpost column
        TableColumn<Paketti, String> endpostcolumn = new TableColumn<>("Saapumisposti");
        endpostcolumn.setMinWidth(300);
        endpostcolumn.setCellValueFactory(new PropertyValueFactory<Paketti, String>("endpost"));

        // Reitin pituus column
        TableColumn<Paketti, String> lengthcolumn = new TableColumn<>("Reitin pituus (km)");
        lengthcolumn.setMinWidth(150);
        lengthcolumn.setCellValueFactory(new PropertyValueFactory<Paketti, String>("matkanpituus"));

        tablev = new TableView();
        tablev.setItems(main.getPacketsList());
        tablev.getColumns().addAll(esinecolumn, startpostcolumn, endpostcolumn, lengthcolumn);

        vbox.getChildren().addAll(tablev);

        packetAmount.setText("Pakettien lukumäärä:\t" + main.getPacketAmount());
        packetAmount.setStyle("-fx-font-size: 18px; -fx-font-family: Serif");
    }

}
