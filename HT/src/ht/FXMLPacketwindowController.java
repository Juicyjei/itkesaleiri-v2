/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author n1682
 */
public class FXMLPacketwindowController implements Initializable {

    MainClass main = MainClass.getInstance();

    @FXML
    private ComboBox<String> setcity;
    @FXML
    private ComboBox<String> arrivalcity;
    @FXML
    private ComboBox<String> startsmartpost;
    @FXML
    private ComboBox<String> endsmartpost;
    @FXML
    private ChoiceBox<String> pickobject;
    @FXML
    private TextField objectname;
    @FXML
    private Slider korkeus;
    @FXML
    private TextField objectmass;
    @FXML
    private CheckBox objectbrokenboolean;
    @FXML
    private Slider leveys;
    @FXML
    private Slider syvyys;
    @FXML
    private Slider packetclass;
    @FXML
    private Button closeButton;
    @FXML
    private Pane kuutio;
    @FXML
    private TextArea PacketSizeInfo;
    @FXML
    private Label packetSizeWarning;
    @FXML
    private Label warningText;
    @FXML
    private WebView invisiblemap;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        invisiblemap.getEngine().load(getClass().getResource("Kartta.html").toExternalForm());
        setcity.setItems(main.getCitys());
        arrivalcity.setItems(main.getCitys());
        pickobject.setItems(main.getItemList());
        pickobject.getSelectionModel().selectFirst();

        // Asettaa kuution näkyviin.
        ShowCube cube = new ShowCube();
        kuutio.getChildren().addAll(cube.makeCube(40, 40, 40, 125));
        kuutio.getChildren().addAll(cube.makeClassCube(50, 50, 50));
        kuutio.getChildren().addAll(cube.makeClassCube(125, 125, 125));

    }

    @FXML
    // Avaa luokkainfo FXML:n.
    private void getClassInfoAction(ActionEvent event) {
        try {
            Stage window = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("FXMLClassInfo.fxml"));

            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void selectSmartPost(ActionEvent event) {

        String paikkakunta = setcity.getSelectionModel().getSelectedItem();
        ObservableList<String> ol = main.getSmartPost(paikkakunta);
        startsmartpost.setItems(ol);

    }

    @FXML
    private void selectSmartPostEnd(ActionEvent event) {
        String paikkakunta = arrivalcity.getSelectionModel().getSelectedItem();
        ObservableList<String> ol = main.getSmartPostEnd(paikkakunta);
        endsmartpost.setItems(ol);
    }

    @FXML
    private void createObjectAction(ActionEvent event) {

        String name = objectname.getText();
        double width1 = leveys.getValue();
        double height1 = korkeus.getValue();
        double lenght1 = syvyys.getValue();
        String width = Double.toString(width1);
        String height = Double.toString(height1);
        String lenght = Double.toString(lenght1);
        String weight = objectmass.getText();
        boolean broken = objectbrokenboolean.isSelected();
        // Testaa onko uuden Esineen luodessa koko liian suuri kyseiseen pakettiluokkaan nähden.
        if (!(objectname.getText().equals("")) && !(objectmass.getText().equals(""))) {
            if (packetclass.getValue() == 2) {
                if ((leveys.getValue() > 50) || (korkeus.getValue() > 50) || (syvyys.getValue() > 50)) {
                    packetSizeWarning.setText("Esineen koko on liian suuri!");
                    packetSizeWarning.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
                } else {
                    main.addUsersItems(name, lenght, width, height, weight, broken);
                    pickobject.setItems(main.getItemList());
                    pickobject.getSelectionModel().selectFirst();
                    packetSizeWarning.setText("Esineen luonti onnistui.");
                    packetSizeWarning.setStyle("-fx-text-fill: green; -fx-font-size: 14px;");
                }
            } else {
                if ((leveys.getValue() > 125) || (korkeus.getValue() > 125) || (syvyys.getValue() > 125)) {
                    packetSizeWarning.setText("Esineen koko on liian suuri!");
                    packetSizeWarning.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
                } else {
                    main.addUsersItems(name, lenght, width, height, weight, broken);
                    pickobject.setItems(main.getItemList());
                    pickobject.getSelectionModel().selectFirst();
                    packetSizeWarning.setText("Esineen luonti onnistui.");
                    packetSizeWarning.setStyle("-fx-text-fill: green; -fx-font-size: 14px;");
                }
            }
        } else {
            packetSizeWarning.setText("Et antanut kaikkia tietoja!");
            packetSizeWarning.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
        }

    }

    @FXML
    private void getClassValueAction(MouseEvent event) {
        ShowCube cube = new ShowCube();
        double valueY = korkeus.getValue();
        double valueX = leveys.getValue();
        double valueZ = syvyys.getValue();
        double value = packetclass.getValue();
        int cvalues = (int) value;
        kuutio.getChildren().clear();
        // Testaa luokkasliderin arvon ja ei anna luoda pakettia, joka ei vastaisi luokkataulun kokovaatimuksia.
        if (cvalues == 2) {
            kuutio.getChildren().addAll(cube.makeCube(valueX, valueY, valueZ, 50));
        } else {
            kuutio.getChildren().addAll(cube.makeCube(valueX, valueY, valueZ, 125));
        }
        kuutio.getChildren().addAll(cube.makeClassCube(50, 50, 50));
        kuutio.getChildren().addAll(cube.makeClassCube(125, 125, 125));
    }

    @FXML
    private void createPackectAction(ActionEvent event) {
        // Testaa onko postiautomaattia valittu.
        if ((startsmartpost.getSelectionModel().isEmpty()) || (endsmartpost.getSelectionModel().isEmpty())) {
            warningText.setText("Anna postiautomaatti");
            warningText.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
        } else {

            Double objectclas = packetclass.getValue();
            int objectclass = objectclas.intValue();
            System.out.println(objectclass);
            String esinenimi = pickobject.getSelectionModel().getSelectedItem();
            String startpost = startsmartpost.getSelectionModel().getSelectedItem();
            String endpost = endsmartpost.getSelectionModel().getSelectedItem();
            // Paketinluontipaneeliin luotu näkymätön kartta, jonka avulla pituudenlaskuscripti on toteutettu.
            String s = "document.laskePituus(" + main.getPathArray(startpost, endpost).toString() + ")";
            double pituus = Double.valueOf(invisiblemap.getEngine().executeScript(s).toString());
            System.out.println(pituus);
            main.getEsineId(esinenimi);

            //Ei anna luoda pakettia, jos luokka ja matkan pituusvaatimukset eivät ole täyttyneet.
            if ((objectclas == 1) && (pituus > 150)) {
                warningText.setText("Pakettia ei luotu, matka liian pitkä.\nVaihda pakettiluokkaa.");
                warningText.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
            } else if (startpost.equals(endpost)) {
                warningText.setText("Lähtö ja saapumispaikka ei voi olla samat.");
                warningText.setStyle("-fx-text-fill: red; -fx-font-size: 14px;");
            } else if (pickobject.getSelectionModel().isEmpty()) {
                    System.out.println("Laita esine...");
            } else {
                // Luodaan paketti.
                main.createPacket(objectclass, esinenimi, startpost, endpost, pituus);
                main.getCreatedPackets();
                warningText.setText("Paketin luonti onnistui.");
                warningText.setStyle("-fx-text-fill: green; -fx-font-size: 14px;");
            }
        }
    }

    @FXML
    private void closeWindowAction(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();

    }

    @FXML
    private void deleteItemAction(ActionEvent event) {
        String esinenimi = pickobject.getSelectionModel().getSelectedItem();
        main.deleteSelectedItem(esinenimi);
        pickobject.setItems(main.getItemList());
    }

    // Luo kuution
    private void setCubeSize() {
        ShowCube cube = new ShowCube();
        double valueY = korkeus.getValue();
        double valueX = leveys.getValue();
        double valueZ = syvyys.getValue();
        double value = packetclass.getValue();
        int cvalues = (int) value;
        kuutio.getChildren().clear();
        if (cvalues == 2) {
            kuutio.getChildren().addAll(cube.makeCube(valueX, valueY, valueZ, 50));
        } else {
            kuutio.getChildren().addAll(cube.makeCube(valueX, valueY, valueZ, 125));
        }
        kuutio.getChildren().addAll(cube.makeClassCube(50, 50, 50));
        kuutio.getChildren().addAll(cube.makeClassCube(125, 125, 125));
    }

    // Jokaiselle koordinaattiakselille kuution muuttamiseen luotu oma action.
    @FXML
    private void setCubeSizeX(MouseEvent event) {
        setCubeSize();

    }

    @FXML
    private void setCubeSizeY(MouseEvent event) {
        setCubeSize();

    }

    @FXML
    private void setCubeSizeZ(MouseEvent event) {
        setCubeSize();
    }
}
