/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

/**
 *
 * @author n1682
 */
public class Smartpost {
    
    private final String city;
    private final String address;
    private final String postalcode;
    private final String availability;
    private final String postoffice;
    private final geoPoint geopoint;
    
    public Smartpost(String city, String address,String code,String availability, String postoffice, String lat, String lng){
        this.city = city;
        this.address = address;
        postalcode = code;
        this.availability = availability;
        this.postoffice = postoffice;
        geopoint = new geoPoint(lat,lng);
        
    }
    // Sijainti aliluokka
        class geoPoint{
            
            private final String lat;
            private final String lng;   
            
            public geoPoint(String lat, String lng){
                this.lat = lat;
                this.lng = lng;
            }
            public String getLat(){
                return lat;
            }
            public String getLng(){
                return lng;
            }
        }

    public String getCity(){
        return city;
    }
    
    public String getAddress(){
        return address;
    }
    public String getPostalcode(){
        return postalcode;
    }
    public String getAvailability(){
        return availability;
    }
    public String getPostOffice(){
        return postoffice;
    }
    public geoPoint getGeoPoint(){
        return geopoint;
    }
}
