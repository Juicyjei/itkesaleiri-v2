/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

public class Point {

    private String name;
    private static boolean b = true;
    private AnchorPane panel;
    private static double x1, y1, x2, y2;

    public Point(AnchorPane panel) {
        this.panel = panel;
    }

    public Circle getCircle(final double x, final double y) {
        final ShapeHandler sh = ShapeHandler.getInstance();
        Circle c = new Circle();
        c.setCenterX(x);
        c.setCenterY(y);

        c.setRadius(10);
        c.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                event.consume();
                System.out.println("Tämä on circel");
                System.out.println("X: " + event.getX() + " , Y: " + event.getY());
                System.out.println("toimiiko?");
                if (b == true) {

                    x1 = x;
                    y1 = y;
                    System.out.println("x = " + x1 + "y = " + y1);
                    System.out.println(b);
                    b = false;
                } else {

                    x2 = x;
                    y2 = y;
                    System.out.println(b);
                    System.out.println("x = " + x2 + "y = " + y2);

                    b = true;
                    panel.getChildren().add(sh.drawline(x1, y1, x2, y2));      
                }

            }

        });

        sh.getList().add(c);

        return c;
    }

    public double getX() {
        return x1;
    }

    public double getY() {
        return y1;
    }

}
