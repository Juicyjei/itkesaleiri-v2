/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author n1682
 */

// Luokka, joka on yhteys GUI ja tietokannan välillä.
public class MainClass {
    static private MainClass main;
    
    Parsecontent parsecontent = Parsecontent.getInstance();
    Tietokanta tietokanta = Tietokanta.getInstance();
    
    public static MainClass getInstance() {
        if (main == null) {
            main = new MainClass();

        }
        return main;
    }
    
    public ObservableList<Paketti> getTableView(){
        ObservableList<Paketti> tableviewlist = FXCollections.observableArrayList();
        return tableviewlist;
    }
    public String getPacketAmount(){
        return tietokanta.getPacketCount();    
    }
    
    public Boolean getBrokenPacket(String esinenimi,int pakettiID){
       if(tietokanta.getBrokenPacket(esinenimi, pakettiID)){
           System.out.println("menirikki");
           return true;
       } else {
           System.out.println("lähetys");
           return false;
       }
    }
    // Lisää default esineet ohjelmaan.
    public void addItemData(){
        tietokanta.addDefaultItem();
    }
    public void addUsersItems(String name,String lenght, String width, String height, String weight, boolean broken){
        tietokanta.addUsersItem(name, lenght, width, height, weight, broken); 
    }
   /* public void addClassDatabase(){
        tietokanta.createClassDb();
    }*/
    
    public void deleteSelectedPacket(int pakettiID){
        tietokanta.deletePacket(pakettiID);
    }
    
    public void deleteSelectedItem(String esinenimi){
        tietokanta.deleteItem(esinenimi);
    }
    public void getEsineId(String esineID){
        tietokanta.getEsineID(esineID);
    }
    public void createPacket(int classobject, String esinenimi,String startpost,String endpost,double matkanpituus){
        tietokanta.createPacket(classobject, esinenimi, startpost, endpost,matkanpituus);
    }
    public void getCreatedPackets(){
        tietokanta.getCreatedPackets();
    }

    public ObservableList<String> getCitys(){
        return parsecontent.getCityList();
    }
    public ArrayList<String> getSmartPostInfo(String paikkakunta){
           tietokanta.getSmartPostInfo(paikkakunta);
           return tietokanta.getInfoList();
    }
    public ArrayList<String> getSearchList(String paikkakunta){
           tietokanta.getSmartPost(paikkakunta);
           return tietokanta.getSearchList();
    }
    public ObservableList<String> getSmartPost(String paikkakunta){
        return tietokanta.getSmartPostByCity(paikkakunta);
    }
    public ObservableList<String> getSmartPostEnd(String paikkakunta){
        return tietokanta.getSmartPostByCityEnd(paikkakunta);
    }
    public ObservableList<String> getItemList(){
        tietokanta.searchItems();
        return tietokanta.getItemList();
    }
    public ArrayList<String> getPathArray(String startpost,String endpost){
           tietokanta.createPathArrayList(startpost, endpost);
           return tietokanta.getPathList();
    }
    public ObservableList<Paketti> getPacketsList(){
        tietokanta.getCreatedPackets();
        return tietokanta.getPacketList();
    }
}
