CREATE TABLE IF NOT EXISTS "sijainti" (
	"sijaintiID" INTEGER 	PRIMARY KEY,
	"postinumero" INTEGER 	NOT NULL,
	"paikkakunta" VARCHAR(32) 	NOT NULL,
	"osoite" 	VARCHAR(32) 	NOT NULL,
	"latitude"		CHAR (10)	NOT NULL,
	"longitude"		CHAR (10)	NOT NULL,
	
	CHECK ("sijaintiID" > 0)

);


CREATE TABLE IF NOT EXISTS"postiautomaatti" (
	"automaattiID"	INTEGER		PRIMARY KEY,
	"sijaintiID"	INTEGER		NOT NULL,
	"aukioloaika"	VARCHAR(64)	NOT NULL,
	"postoffice"	VARCHAR(64)	NOT NULL,

	FOREIGN KEY	("sijaintiID") REFERENCES "sijainti" ("sijaintiID") ON DELETE CASCADE,

	CHECK ("automaattiID" > 0)
);

CREATE TABLE IF NOT EXISTS"luokka" (
	"luokkaID"	INTEGER 	PRIMARY KEY,
	"matkaraja" INTEGER,
	"painoraja" INTEGER,
	"kokorajaX" INTEGER,
	"kokorajaY" INTEGER,
	"kokorajaZ" INTEGER,

	CHECK ("painoraja" > 0),
	CHECK ("matkaraja" > 0)

);

CREATE TABLE IF NOT EXISTS"esine" (
	"esineID"	INTEGER		PRIMARY KEY,
	"nimi"		VARCHAR(32) NOT NULL,
	"korkeus" 	INTEGER,
	"leveys"	INTEGER,
	"syvyys"	INTEGER,
	"paino"		INTEGER,
	"getbroken"	BOOLEAN,

	CHECK ("esineID" > 0),
	CHECK ("leveys" > 0),
	CHECK ("korkeus" > 0),
	CHECK ("syvyys" > 0),
	CHECK ("paino" > 0)
);

CREATE TABLE IF NOT EXISTS"paketti" (
	"pakettiID"	INTEGER 	PRIMARY KEY,
	"esineID"	INTEGER,
	"luokkaID"	INTEGER,
	"startpost" VARCHAR(32),
	"endpost"	VARCHAR(32), 
        "matkanpituus" REAL,
	
	FOREIGN KEY ("startpost") REFERENCES "postiautomaatti" ("automaattiID") ON DELETE CASCADE,
	FOREIGN KEY ("endpost") REFERENCES "postiautomaatti" ("automaattiID") ON DELETE CASCADE,
	FOREIGN KEY ("esineID") REFERENCES "esine" ("esineID" ) ON DELETE CASCADE,
	FOREIGN KEY ("luokkaID") REFERENCES "luokka" ("luokkaID") ON DELETE CASCADE,

	CHECK ("pakettiID" > 0),
	CHECK ("esineID" > 0),
	CHECK ("luokkaID" > 0)
	
);

CREATE TABLE IF NOT EXISTS"paketitautomaatissa" (
	"pakettiID"	INTEGER,
	"automaattiID"	INTEGER,

	PRIMARY KEY ("pakettiID", "automaattiID")
);


CREATE TABLE IF NOT EXISTS"varasto" (
	"pakettiID" INTEGER PRIMARY KEY,
	"varastoID"	VARCHAR,

	FOREIGN KEY ("pakettiID") REFERENCES "paketti" ("pakettiID") ON DELETE CASCADE
);