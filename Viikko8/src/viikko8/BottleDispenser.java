/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8;

import java.util.ArrayList;
import java.util.List;

public class BottleDispenser {

    private int bottles;
    private double money;
    private List<Bottle> pullolista;
    Bottle bottle = new Bottle();
    static private BottleDispenser bd;
    String a;
    int b;

    private BottleDispenser() {
        bottles = 6;
        money = 0;
        pullolista = new ArrayList<Bottle>();
        String n;
        double t, h;
        pullolista.add(new Bottle("Pepsi Max", "0.5", 1.8));
        pullolista.add(new Bottle("Pepsi Max", "1.5", 2.2));
        pullolista.add(new Bottle("Coca-Cola Zero", "0.5", 2.0));
        pullolista.add(new Bottle("Coca-Cola Zero", "1.5", 2.5));
        pullolista.add(new Bottle("Fanta Zero", "0.5", 1.95));
        pullolista.add(new Bottle("Fanta Zero", "0.5", 1.95));

    }

    public static BottleDispenser getInstance() {
        if (bd == null) {
            bd = new BottleDispenser();
        }
        return bd;
    }

    public void listaapullot() {
        for (int i = 0; i < bottles; i++) {
            System.out.println((i + 1) + ". Nimi: " + pullolista.get(i).getName() + "\n" + "\tKoko: " + pullolista.get(i).getSize() + "\tHinta: " + pullolista.get(i).getMoney());
        }
    }

    public void addMoney(double raha) {
        money += raha;
        System.out.println("Klink! Lisää rahaa laitteeseen!" + raha);
    }

    public void buyBottle(String limu, String koko) {

        for (int i = 0; i < bottles; i++) {
            if (limu.equals(pullolista.get(i).getName()) == true) {

                if (koko.equals(pullolista.get(i).getSize()) == true) {
                    if (money >= pullolista.get(i).getMoney()) {
                        b = 0;
                        System.out.println("KACHUNK! " + pullolista.get(i).getName() + " tipahti masiinasta!");
                        money -= pullolista.get(i).getMoney();
                        bottles -= 1;
                        bottle = pullolista.get(i);
                        pullolista.remove(i);
                        return;
                    }else{
                        b = 2;
                        return;
                    }
                }
            }
        }
        b = 1;
    }

    public int getBottles() {
        return bottles;
    }

    public double getMoney() {
        return money;
    }

    public void seMoney() {
        money = 0;
    }

    public List<Bottle> getPullolista() {
        return pullolista;
    }

    public Bottle getBottle() {
        return bottle;
    }

    public String getVirhe() {
        return a;
    }

    public int getNumber() {
        return b;
    }

}
