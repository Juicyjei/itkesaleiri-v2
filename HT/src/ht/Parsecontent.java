/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author n1682
 */
public class Parsecontent {
    
    private Document doc;
    private ArrayList<String> citys;
    private ArrayList<Smartpost> Smartpost;
    static private Parsecontent parsecontent;
    
    private Parsecontent(){
        
        Smartpost = new ArrayList();
        citys = new ArrayList();
        
    }
    
    public static Parsecontent getInstance() {
        if (parsecontent == null) {
            parsecontent = new Parsecontent();
        }
        return parsecontent;
    }
    
    
    public String getContentAction(){

        try {
            //Sijainti mistä XML data
            String path = String.format("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
            URL url = new URL(path);
            
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
            
            String rivi;
            String content1 = "";
            
            while ((rivi = br.readLine()) != null){
                content1 += rivi + "\n";
            }

            return content1;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Parsecontent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "virhe";
    }
    
    public void documentfactory(String content1) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content1)));
            doc.getDocumentElement().normalize();
            //suorittaa parseamisen
            parseContent();

            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Parsecontent.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    // XML muotoinen data parsitaan "item" tagin mukaan. Siirretään saadut tiedot smartpost luokkaan oloihin.
    private void parseContent() {
        NodeList nodes = doc.getElementsByTagName("item");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Node city = e.getElementsByTagName("city").item(0);
                city.getTextContent();
                Node address = e.getElementsByTagName("address").item(0);
                address.getTextContent();
                Node postalcode = e.getElementsByTagName("postalcode").item(0);
                postalcode.getTextContent();
                Node availability = e.getElementsByTagName("availability").item(0);
                availability.getTextContent();
                Node postoffice = e.getElementsByTagName("name").item(0);
                postoffice.getTextContent();
                Node lat = e.getElementsByTagName("lat").item(0);
                lat.getTextContent();
                Node lng = e.getElementsByTagName("lng").item(0);
                lng.getTextContent();
                //Lisää smartpost olioon xml parseamis tiedot.
                Smartpost.add(new Smartpost(city.getTextContent(),address.getTextContent(),postalcode.getTextContent(),
                        availability.getTextContent(),postoffice.getTextContent(),lat.getTextContent(),lng.getTextContent()));
                
                citys.add(city.getTextContent());
            }
        }
    }
    
    // Palauttaa listan, joka poistaa dublicatit.
    public ArrayList<String> removeDublicate(ArrayList<String> citys){
        ArrayList<String> result = new ArrayList<>();
        HashSet<String> set = new HashSet<>();
        
        for(String i : citys){       
            if(!set.contains(i)){
                result.add(i);
                set.add(i);
            }
        }
        return result;
    }
      
    public ObservableList<String> getCityList(){
        ArrayList<String> uniquecitys = removeDublicate(citys);
        ObservableList<String> uniquecitys1 = FXCollections.observableArrayList(uniquecitys);
        java.util.Collections.sort(uniquecitys1);
        return uniquecitys1;  
    }
    public ArrayList<Smartpost> getSmartPostList(){
        return Smartpost;
    }
    
}
