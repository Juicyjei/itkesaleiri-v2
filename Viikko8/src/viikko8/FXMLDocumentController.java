
package viikko8;
// Jussi Moilanen
// 5.6.2018
// gui Bottledispenser finished 

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

/**
 *
 * @author n1682
 */
public class FXMLDocumentController implements Initializable {
    BottleDispenser pullokone = BottleDispenser.getInstance();

    @FXML
    private Font x1;  
    @FXML
    private TextArea textarea;
    @FXML
    private Label money;
    @FXML
    private Slider slider;
    @FXML
    private ComboBox<String> dropdownlimu;
    @FXML
    private ComboBox<String> dropdownkoko;
    @FXML
    private TextArea textarea2;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dropdownlimu.getSelectionModel().selectFirst();
        dropdownkoko.getSelectionModel().selectFirst();
        money.setText("Lisää koneeseen 0.00 euroa.");
        textarea2.setText("Pepsi Max 0.5l 1.8e\nPepsi Max 1.5 2.2e\nCoca-Cola Zero 0.5l 2e\nCoca-Cola Zero 1.5l 2.5e\nFanta Zero 0.5l 1.95e\nFanta Zero 0.5l 1.95e");
        
    }

    @FXML
    private void addMoneyButton(ActionEvent event) {
        pullokone.addMoney(slider.getValue());
        textarea.setText(String.format("Klink! Lisää rahaa laitteeseen!\nRahaa on %.1f euroa.\n"  , pullokone.getMoney()));
        slider.setValue(0);
        money.setText(String.format("Lisää koneeseen %.1f ", slider.getValue()) +" euroa.");
        
    }

    @FXML
    private void buyButton(ActionEvent event) {
        //pullokone.buyBottle(dropdown.getValue());
  
            pullokone.buyBottle(dropdownlimu.getValue(),dropdownkoko.getValue());
            if (pullokone.getNumber() == 1){
                System.out.println("virhe");
                textarea.setText("Valitettavasti pulloa ei löydy valikoimastamme. :(");
            }
            else if (pullokone.getNumber() == 2){
                   textarea.setText("Not enough money my mate...");
                   
            }
            else{
                textarea.setText("Osto onnistui, pullo tulossa...");
            }
            

        textarea2.clear();
        for( int i = 0; i < pullokone.getBottles(); i++){
            textarea2.appendText((i + 1) + ". Nimi: " + pullokone.getPullolista().get(i).getName() + " Koko: " + pullokone.getPullolista().get(i).getSize() + " Hinta: " + pullokone.getPullolista().get(i).getMoney()+ "\n");

        }   
        //textarea.setText(String.format("Klink! Lisää rahaa laitteeseen!\nRahaa on %.1f euroa.\n"  , pullokone.getMoney()));
    }
    @FXML
    private void sliderChange(MouseEvent event) {
        money.setText(String.format("Lisää koneeseen %.1f ", slider.getValue()) +" euroa.");
        
    }
    @FXML
    private void restoreMoney(ActionEvent event){
        
        textarea.setText(String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n",pullokone.getMoney()));
        slider.setValue(0);
        pullokone.seMoney();
        
        
    }

    @FXML
    private void writeFile(ActionEvent event) {
        Bottle b = pullokone.getBottle();
        try {
            BufferedWriter w = new BufferedWriter(new FileWriter("tulokset.txt"));
                w.write("Kuitti, viimeinen ostos: "+ b.getName() +" " + b.getMoney()+ "e");
                textarea.clear();
            w.close();
        } catch (FileNotFoundException ex){
                System.out.println("Tiedostoa ei löytynyt.");
        } catch (IOException ex){
            System.err.println("Tiedoston luku ei onnistunut."); 
        }
    }

}
