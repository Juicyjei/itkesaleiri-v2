/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author n1682
 */
public final class Tietokanta {

    static private Tietokanta tietokanta;
    private Parsecontent parsecontent;
    private Connection conn = null;

    ArrayList<String> searchlist = new ArrayList();
    ArrayList<String> infolist = new ArrayList();
    ArrayList<String> pathlist = new ArrayList();
    ObservableList<String> startSmartPost = FXCollections.observableArrayList();
    ObservableList<String> startSmartPostEnd = FXCollections.observableArrayList();
    ObservableList<String> itemlist = FXCollections.observableArrayList();
    ObservableList<Paketti> packetslist = FXCollections.observableArrayList();

    private Tietokanta() {

        parsecontent = Parsecontent.getInstance();
        parsecontent.documentfactory(parsecontent.getContentAction());
        // Heti ohjelman käynnistyessä yhteys luodaaan ja tietokantaan lisätään dataa.
        try {
            // db parameters
            String url = "jdbc:sqlite:HT2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");
            try {
                Statement st = conn.createStatement();
                st.executeQuery("SELECT * FROM sijainti WHERE id = -1;");
                // Jos ei tauluja ole luotu lisää catchissä smartpostit databaseen.
            } catch (SQLException ex) {
                createTables();
                addSmartPostDatabase();
                createClassDb();
                System.out.println("Lisätty data tietokantaan.");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    @SuppressWarnings("FinalizeDeclaration")
    protected void finalize() {
        try {
            super.finalize();
            if (conn != null) {
                conn.commit();
                conn.close();
            }
        } catch (Throwable ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Singelton
    public static Tietokanta getInstance() {
        if (tietokanta == null) {
            tietokanta = new Tietokanta();

        }
        return tietokanta;
    }
    
    public void createTables(){
        String s;
        try (Statement st = conn.createStatement()) {
            
            File file = new File("HT1.sql");
            System.out.println("Tiedostopolku: " + file.getAbsolutePath());
            
            try (BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()))) {
                StringBuffer sb = new StringBuffer();
                while ((s = br.readLine()) != null) {
                    sb.append(s + "\n");
                    
                }
                st.executeUpdate(sb.toString());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("HT1.sql ei löytynyt.");
        } catch (IOException ex) {
            System.err.println("Tiedoston luku ei onnistunut.");
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Lisää sijainti sekä smartpostiedot, jotka saadaan parsecontent luokan palauttamista listoista.
    public void addSmartPostDatabase() {

        try (Statement st = conn.createStatement()) {

            for (int i = 0; i < parsecontent.getSmartPostList().size(); i++) {

                st.executeUpdate("INSERT INTO sijainti" + " VALUES (" + (i + 1) + ","
                        + parsecontent.getSmartPostList().get(i).getPostalcode() + ",'"
                        + parsecontent.getSmartPostList().get(i).getCity() + "','"
                        + parsecontent.getSmartPostList().get(i).getAddress() + "','"
                        + parsecontent.getSmartPostList().get(i).getGeoPoint().getLat() + "','"
                        + parsecontent.getSmartPostList().get(i).getGeoPoint().getLng() + "')");
                st.executeUpdate(String.format("INSERT INTO postiautomaatti VALUES (%s,%s,'%s','%s')", (i + 1), (i + 1),
                        parsecontent.getSmartPostList().get(i).getAvailability(), parsecontent.getSmartPostList().get(i).getPostOffice()));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Tiedot on jo lisätty");
        }
    }

    public void addDefaultItem() {
        try (Statement st = conn.createStatement()) {

            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 1, "ONE PLUS 6 128GB", 8, 8, 15, 1, "TRUE"));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 2, "Perry Miesten Canvas kengät", 15, 15, 30, 1, "FALSE"));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 3, "Macbook Pro 13 Tähtiharmaa", 8, 30, 30, 2, "TRUE"));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%d,'%s',%d,%d,%d,%d,%s)", 4, "Pioneer SX-S30DAB Pre-Out + Genelec G Three", 15, 15, 20, 3, "TRUE"));

        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println("Esineet on jo lisätty");
        }
    }

    public void addUsersItem(String name, String lenght, String width, String height, String weight, boolean broken) {
        try (Statement st = conn.createStatement()) {
            st.executeUpdate(String.format("INSERT OR IGNORE INTO esine VALUES "
                    + "(%s,'%s',%s,%s,%s,%s,%b)", null, name, lenght, width, height, weight, broken));

        } catch (SQLException ex) {
            //Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Esinettä ei lisätty");
        }
    }

    public void createClassDb() {
        try (Statement st = conn.createStatement()) {
            st.executeUpdate(String.format("INSERT OR IGNORE INTO luokka VALUES "
                    + "(%s,%s,%s,%s,%s,%s)", null, 150, 250, 125, 125, 125));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO luokka VALUES "
                    + "(%s,%s,%s,%s,%s,%s)", null, 1500, 25, 50, 50, 50));
            st.executeUpdate(String.format("INSERT OR IGNORE INTO luokka VALUES "
                    + "(%s,%s,%s,%s,%s,%s)", null, 1500, 250, 125, 125, 125));

        } catch (SQLException ex) {
            //Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Luokkatietoja ei lisätty");
        }
    }

    public void searchItems() {
        try {
            itemlist.clear();
            Statement st;
            String query = "SELECT nimi from esine;";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String osoite = rs.getString(1);
                itemlist.add(osoite);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteItem(String esinenimi) {
        try {
            Statement st = conn.createStatement();
            String query = "delete from esine where nimi = '" + esinenimi + "';";
            st.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //poistaa paketti -ja varastotaulusta pakettiID:n perusteella. 
    public void deletePacket(int pakettiID) {
        try {
            Statement st = conn.createStatement();
            String query = "delete from paketti where pakettiID = '" + pakettiID + "';";
            String query1 = "delete from varasto where pakettiID = '" + pakettiID + "';";
            st.executeUpdate(query);
            st.executeUpdate(query1);
            System.out.println("Paketti poistettu");
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getEsineID(String esinenimi) {
        try {
            Statement st;
            String query = "SELECT esineID FROM esine WHERE nimi = '" + esinenimi + "';";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            String esineid = rs.getString(1);
            return esineid;
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "virhe";
    }

    public String getPacketCount() {
        try {
            Statement st;
            String query = "select COUNT(*) from paketti;";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            String packetAmount = rs.getString(1);
            return packetAmount;
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "virhe";
    }

    public void createPacket(int classobject, String esinenimi, String startpost, String endpost, double matkanpituus) {
        try (Statement st = conn.createStatement()) {

            if (startpost == null || endpost == null) {
                System.out.println("Et antanut kaikkia tietoja.");
            } else {
                st.executeUpdate(String.format("INSERT INTO paketti VALUES "
                        + "(%s,%s,%s,'%s','%s',%s)", null, getEsineID(esinenimi), classobject, startpost, endpost, matkanpituus));
                String lastrowid = "last_insert_rowid()";
                st.executeUpdate(String.format("INSERT OR IGNORE INTO varasto VALUES "
                        + "(%s,%s)", lastrowid, 1));
                System.out.println("Paketti lisätty varastoon");
            }
        } catch (SQLException ex) {
            //Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Pakettitietoja ei lisätty");
        }

    }

    public void createPathArrayList(String startpost1, String endpost1) {
        try {
            // Pathlist sisältää Arrayn jossa postitoimpaikan lähtö ja loppukoordinaatit. 
            pathlist.clear();
            Statement st;
            String query = "SELECT latitude, longitude  from sijainti INNER JOIN"
                    + " postiautomaatti ON sijainti.sijaintiID = postiautomaatti."
                    + "sijaintiID WHERE postoffice = '" + startpost1 + "';";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pathlist.add(rs.getString(1));
                pathlist.add(rs.getString(2));
            }
            String query2 = "SELECT latitude, longitude  from sijainti INNER JOIN"
                    + " postiautomaatti ON sijainti.sijaintiID = postiautomaatti."
                    + "sijaintiID WHERE postoffice = '" + endpost1 + "';";
            Statement st1;
            st1 = conn.createStatement();
            ResultSet rs2 = st1.executeQuery(query2);
            while (rs2.next()) {
                pathlist.add(rs2.getString(1));
                pathlist.add(rs2.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getSmartPost(String paikkakunta) {
        try {
            Statement st;
            String query = "SELECT osoite, postinumero FROM sijainti WHERE paikkakunta = '" + paikkakunta + "';";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String osoite = rs.getString(1) + "," + rs.getInt(2);
                searchlist.add(osoite);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Boolean getBrokenPacket(String esinenimi, int pakettiID) {
        try {
            Statement st;
            String query = "SELECT getbroken FROM esine"
                    + " WHERE nimi = '" + esinenimi + "';";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            int broken = rs.getInt(1);
            System.out.println(broken);
            //
            Statement st1;
            String query1 = "SELECT luokkaID FROM paketti"
                    + " WHERE pakettiID = '" + pakettiID + "';";
            st1 = conn.createStatement();
            ResultSet rs1 = st1.executeQuery(query1);
            int luokkaID = rs1.getInt(1);
            //
            Statement st2;
            String query2 = "SELECT paino FROM esine"
                    + " WHERE nimi = '" + esinenimi + "';";
            st2 = conn.createStatement();
            ResultSet rs2 = st2.executeQuery(query2);
            int mass = rs2.getInt(1);

            // Paketti menee rikki, jos paketilla on ominaisuus mennä rikki tosi ja muut ehdot täyttyvät.
            if (((broken == 1) && (luokkaID == 1)) || (((broken == 1) && (mass < 20)) && (luokkaID == 3))) {
                System.out.println("Paketti meni rikki.");
                return true;
            } else {
                System.out.println("Paketti meni perille.");
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void getSmartPostInfo(String paikkakunta) {
        try {
            Statement st;
            String query = "SELECT aukioloaika, postoffice FROM postiautomaatti "
                    + "INNER JOIN sijainti ON sijainti.sijaintiID = "
                    + "postiautomaatti.sijaintiID WHERE paikkakunta = '" + paikkakunta + "';";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String tiedot = rs.getString(1) + "," + rs.getString(2);
                infolist.add(tiedot);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getCreatedPackets() {
        try {
            packetslist.clear();
            Statement st;
            String query = "SELECT nimi, pakettiID, startpost, endpost, matkanpituus from esine INNER JOIN paketti ON "
                    + "paketti.esineID = esine.esineID WHERE paketti.esineID = esine.esineID;";
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                packetslist.add(new Paketti(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getDouble(5)));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Pakettitietoja ei saatu haettua.");
        }
    }

    //Palauttaa ObListin, joka sisältää smartpostien nimet paikkakunnan perusteella.
    public ObservableList<String> getSmartPostByCity(String paikkakunta) {
        try {
            startSmartPost.clear();
            Statement st;
            String query = String.format("SELECT postoffice from postiautomaatti "
                    + "INNER JOIN sijainti On sijainti.sijaintiID = postiautomaatti.sijaintiID"
                    + " WHERE paikkakunta = '%s'", paikkakunta);
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String tiedot = rs.getString(1);
                startSmartPost.add(tiedot);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return startSmartPost;
    }

    public ObservableList<String> getSmartPostByCityEnd(String paikkakunta) {
        try {
            startSmartPostEnd.clear();
            Statement st;
            String query = String.format("SELECT postoffice from postiautomaatti "
                    + "INNER JOIN sijainti On sijainti.sijaintiID = postiautomaatti.sijaintiID"
                    + " WHERE paikkakunta = '%s'", paikkakunta);
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String tiedot = rs.getString(1);
                startSmartPostEnd.add(tiedot);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tietokanta.class.getName()).log(Level.SEVERE, null, ex);
        }
        return startSmartPostEnd;
    }

    public ArrayList<String> getSearchList() {
        return searchlist;
    }

    public ArrayList<String> getInfoList() {
        return infolist;
    }

    public ObservableList<String> getItemList() {
        return itemlist;
    }

    public ObservableList<Paketti> getPacketList() {
        return packetslist;
    }

    public ArrayList<String> getPathList() {
        return pathlist;
    }
}
