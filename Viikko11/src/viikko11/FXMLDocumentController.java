/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import java.awt.Color;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author n1682
 */
public class FXMLDocumentController implements Initializable {
    
    ShapeHandler sh = ShapeHandler.getInstance();
    
    @FXML
    private AnchorPane panel;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    


    @FXML
    private void baseMouseClicked(javafx.scene.input.MouseEvent event) {
        Point p = new Point(panel);

        double x = (event.getX());
        double y = (event.getY());

        panel.getChildren().add(p.getCircle(x, y));
        
        
                
        System.out.println(sh.getList());
    }
}