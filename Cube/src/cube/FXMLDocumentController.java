/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cube;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Jussi
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Pane panel;
    @FXML
    private Pane kuutio;
    @FXML
    private Slider pickSizeX;
    @FXML
    private Slider pickSizeY;
    @FXML
    private Slider pickSizeZ;
    @FXML
    private CheckBox class1;
    @FXML
    private CheckBox class2;
    @FXML
    private CheckBox class3;
    

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ShowCube cube = new ShowCube();
        panel.setStyle("-fx-background-color: black;");
        kuutio.getChildren().addAll(cube.makeCube(50,50,50));
        kuutio.getChildren().addAll(cube.makeClassCube());
        
    }    

    @FXML
    private void setCubeSizeX(MouseEvent event) {
        ShowCube cube = new ShowCube();
        double valueY = pickSizeY.getValue();
        double valueX = pickSizeX.getValue();
        double valueZ = pickSizeZ.getValue();
        kuutio.getChildren().clear();
        kuutio.getChildren().addAll(cube.makeCube(valueX,valueY,valueZ));
        kuutio.getChildren().addAll(cube.makeClassCube());
    }

    @FXML
    private void setCubeSizeY(MouseEvent event) {
        ShowCube cube = new ShowCube();
        double valueY = pickSizeY.getValue();
        double valueX = pickSizeX.getValue();
        double valueZ = pickSizeZ.getValue();
        kuutio.getChildren().clear();
        kuutio.getChildren().addAll(cube.makeCube(valueX,valueY,valueZ));
        kuutio.getChildren().addAll(cube.makeClassCube());
    }

    @FXML
    private void setCubeSizeZ(MouseEvent event) {
        ShowCube cube = new ShowCube();
        double valueY = pickSizeY.getValue();
        double valueX = pickSizeX.getValue();
        double valueZ = pickSizeZ.getValue();
        kuutio.getChildren().clear();
        kuutio.getChildren().addAll(cube.makeCube(valueX,valueY,valueZ));
        kuutio.getChildren().addAll(cube.makeClassCube());
    }

    @FXML
    private void class1Value(ActionEvent event) {
    }

    @FXML
    private void class2Value(ActionEvent event) {
    }

    @FXML
    private void class3Value(ActionEvent event) {
    }
    
}
