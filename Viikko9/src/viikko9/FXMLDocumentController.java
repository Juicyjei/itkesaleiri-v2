/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;

/**
 *
 * @author n1682
 */
public class FXMLDocumentController implements Initializable {
    
    Mainclass m = Mainclass.getInstance();
    
    private Label label;
    @FXML
    private ChoiceBox<Theatre> picktheatre;
    @FXML
    private Font x1;
    @FXML
    private TextField actday;
    @FXML
    private TextField startingtime;
    @FXML
    private TextField endingtime;
    @FXML
    private Button listfilms;
    @FXML
    private Font x2;
    @FXML
    private TextField givemoviename;
    @FXML
    private Button namesearch;
    
    private TextArea textarea;
    @FXML
    private ListView<String> textarea1;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        m.mainclass(m.getContentAction());
        
        List<Theatre> lista = m.getTheatre();
        picktheatre.getItems().addAll(lista);
        //ObservableList<Theatre> oblist = m.listToOblist();
       // picktheatre.setItems(oblist);
        picktheatre.getSelectionModel().selectFirst();
    }    

    @FXML
    private void listFilmsAction(ActionEvent event) {
        String valinta = picktheatre.getSelectionModel().getSelectedItem().getID();
        String date = actday.getText();
        String startingaika = startingtime.getText();
        String endingaika = endingtime.getText();
        
        //ObservableList<String> obmovies = FXCollections.observableArrayList(m.getTheatre());
        //textarea1.setItems(obmovies);
        
        
        //m.getTheatherAndDate(valinta, date);
        textarea1.setItems(m.getMovies(valinta, date));
        m.getMovieByTime(startingaika, endingaika);
        
        
        
  
        //picktheatre.getSelectionModel().selectFirst();

      
    }

    

    @FXML
    private void nameSearchAction(ActionEvent event) {
        
        String haku = givemoviename.getText();
        m.getMovieByName(haku);
        ObservableList<String> obmoviesbyname = FXCollections.observableArrayList(m.getMovieListByName());
        textarea1.setItems(obmoviesbyname);
        
    }

    @FXML
    private void empty(ActionEvent event) {
        textarea1.getItems().clear();
        
    }

    @FXML
    private void actDayAction(ActionEvent event) {
    }

    @FXML
    private void startingTimeAction(ActionEvent event) {
    }

    @FXML
    private void endingTimeAction(ActionEvent event) {
    }

    @FXML
    private void giveMovieNameAction(ActionEvent event) {
    }
    
}
